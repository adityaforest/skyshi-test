const activities = require('../../models').activities
const responseFormatter = require('../utils/response.formatter')

module.exports = {
    getAll: async (req,res) => {
        try{
            const activities_db = await activities.findAll()
            res.json(responseFormatter.success(activities_db))
        } catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } , 
    getOne : async (req,res) => {
        const id = req.params.id
        try{
            const activities_db = await activities.findOne({
                where:{
                    id
                }
            })            
            
            if(!activities_db){
                res.status(404).json(responseFormatter.error('Not Found',`Activity with ID ${id} Not Found`))
            }
            else{
                res.json(responseFormatter.success(activities_db))
            }
        } catch (err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } ,
    create : async (req , res) => {
        const {title,email} = req.body
        try{
            if(!title) return res.status(400).json(responseFormatter.error('Bad Request',"title cannot be null"))
            const activities_db = await activities.create({
                title,
                email
            })            
            res.status(201).json(responseFormatter.success(activities_db))
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } , 
    update: async (req,res) => {
        const id = req.params.id
        const {title} = req.body
        try{            
            let activities_db = await activities.findOne({where:{id}})
            if(!activities_db){
                res.status(404).json(responseFormatter.error('Not Found',`Activity with ID ${id} Not Found`))
            }
            else{
                activities_db = await activities.update({title},{where:{id}})
                activities_db = await activities.findOne({where:{id}})
                res.status(200).json(responseFormatter.success(activities_db))
            }            
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } ,
    delete : async (req,res) => {
        const id = req.params.id
        try{
            const activities_db = await activities.findOne({where:{id}})            
            if(!activities_db) return res.status(404).json(responseFormatter.error('Not Found' , `Activity with ID ${id} Not Found`))
            await activities.destroy({where:{id}})
            res.json(responseFormatter.success({}))
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    }
}