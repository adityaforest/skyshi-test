const todos = require('../../models').todos
const responseFormatter = require('../utils/response.formatter')

module.exports = {
    getAll: async (req,res) => {
        const activity_group_id = parseInt(req.query.activity_group_id)
        try{                        
            let todos_db = await todos.findAll()
            if(activity_group_id ) todos_db = await todos.findAll({where:{activity_group_id}})
            res.json(responseFormatter.success(todos_db))
        } catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } , 
    getOne : async (req,res) => {
        const id = req.params.id
        try{
            const todos_db = await todos.findOne({
                where:{
                    id
                }
            })            
            
            if(!todos_db){
                res.status(404).json(responseFormatter.error('Not Found',`Todo with ID ${id} Not Found`))
            }
            else{
                res.json(responseFormatter.success(todos_db))
            }
        } catch (err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } ,
    create : async (req , res) => {
        let {title,activity_group_id,is_active} = req.body
        try{
            if(!title) return res.status(400).json(responseFormatter.error('Bad Request' , "title cannot be null"))
            if(!activity_group_id) return res.status(400).json(responseFormatter.error('Bad Request',"activity_group_id cannot be null"))
            if(is_active != true || is_active != false) is_active = true
            const todos_db = await todos.create({
                title,
                activity_group_id,
                is_active,
                priority:'very-high'
            })            
            res.status(201).json(responseFormatter.success(todos_db))
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } , 
    update: async (req,res) => {
        const id = req.params.id
        const {title,priority,is_active,status} = req.body
        try{            
            let todos_db = await todos.findOne({where:{id}})
            if(!todos_db){
                res.status(404).json(responseFormatter.error('Not Found',`Todo with ID ${id} Not Found`))
            }
            else{
                todos_db = await todos.update({title,priority,is_active},{where:{id}})
                todos_db = await todos.findOne({where:{id}})
                res.json(responseFormatter.success(todos_db))
            }            
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    } ,
    delete : async (req,res) => {
        const id = req.params.id
        try{
            const todo_db = await todos.findOne({where:{id}})
            if(!todo_db) return res.status(404).json(responseFormatter.error('Not Found' , `Todo with ID ${id} Not Found`))
            await todos.destroy({where:{id}})
            res.json(responseFormatter.success({}))
        }catch(err){
            res.status(500).json(responseFormatter.error('Error',err.message))
        }
    }
}