const bcrypt = require('bcrypt')
const { generateToken } = require('../utils/jwt.token.util')
const { responseSuccess, responseError } = require('../utils/response.formatter.util')
const { user } = require('../../models')

module.exports = {
    register: async (req, res) => {
        try {
            const { email, password, firstName, lastName } = req.body

            const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

            if (!emailRegexp.test(email)) {
                res.status(500).json(responseError('Email format not valid'))
            } else {
                const userRegistered = await user.findOne({ where: { email } })

                if (userRegistered) {
                    res.status(500).json(responseError('User already exist'))
                } else {
                    const salt = await bcrypt.genSalt(10)
                    const hashedPassword = await bcrypt.hash(password, salt)

                    const userCreate = await user.create({
                        email,                        
                        password: hashedPassword,
                        firstName,
                        lastName
                    })

                    delete userCreate.dataValues.password

                    res.status(201).json(responseSuccess(userCreate))
                }
            }
        } catch (err) {
            res.status(500).json(responseError('Some error occurred while creating the Player'))
        }
    },
    login: async (req, res) => {
        const { email, password } = req.body

        try {
            const userLogin = await user.findOne({ where: { email } })

            if (!userLogin) {
                res.status(500).json(responseError('User not found'))
            } else {
                const match = await bcrypt.compare(password, userLogin.password);
                if (match) {
                    const token = await generateToken(userLogin.id)

                    const responseUser = {
                        id: userLogin.id,
                        uuid: userLogin.uuid,
                        email: userLogin.email,
                        firstName: userLogin.firstName,
                        lastName: userLogin.lastName,
                        token: "Bearer " + token
                    }

                    res.json(responseSuccess(responseUser))
                } else {
                    res.status(500).json(responseError('Incorrect password'))
                }
            }
        } catch (err) {
            console.log(err.message)
            res.status(404).json(responseError('Login error'))
        }
    }
}