const express = require('express')
const router = express.Router()
const activityController = require('../controllers/activity')

router.get('/' , activityController.getAll)
router.get('/:id' , activityController.getOne)
router.post('/',activityController.create)
router.patch('/:id',activityController.update)
router.delete('/:id',activityController.delete)

module.exports = router