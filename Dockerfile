FROM node:alpine
WORKDIR /usr/code
COPY package.json ./
COPY yarn.lock ./
RUN yarn install
COPY . .
EXPOSE 3030
# RUN yarn sequelize db:create
# RUN yarn sequelize db:migrate
# CMD ["yarn","start"]
ENTRYPOINT [ "./startup.sh" ]
